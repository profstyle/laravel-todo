<?php

namespace App\Http\Controllers;
use session;
use URL;

class AppController extends Controller
{
    public function setlocale($locale='tr'){
        if (!in_array($locale,['tr','en'])){
            $locale='tr';

        }
        Session::put('locale',$locale);
        return redirect(url(URL::previous()));

    }
}
