<?php

namespace App\Http\Controllers;

use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Http\Request;
use App\Models\Program;
class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();
        return view('programs.index',compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $request->validate([
        'program_name'=>'required',
        'program_price'=>'required|integer',
        'program_group'=>'required|integer'
    ]);
    $program = new Program([

        'program_name'=>$request->get('program_name'),
        'program_price'=>$request->get('program_price'),
        'program_group'=>$request->get('program_group')
    ]);
    $program->save();
    return redirect('/programs')->with('success','Program Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = Program::find($id);
        return view('programs.edit',compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'program_name'=>'required',
            'program_price'=>'required|integer',
            'program_group'=>'required|integer'
        ]);
        $program=Program::find($id);
        $program->program_name = $request->get('program_name');
        $program->program_price = $request->get('program_price');
        $program->program_group = $request->get('program_group');
        $program->save();
        return redirect('/programs')->with('success','Program Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);
        $program->delete();
        return redirect('/programs')->with('success','Program Silindi');
    }
}
