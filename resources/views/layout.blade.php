
<head>
    <title>
        Program Projesi - ekle düzenle günelle sil </title>
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">

</head>
<body>
<div class="container">
    <a href="{{route('programs.create')}}">Yeni Program Ekle</a> -
    <a href="{{route('programs.index')}}">Program Listesi</a>-
    <a href="{{route('profile')}}">Profil Bilgileri</a>-
    <a href="{{route('login')}}">Üye Girişi</a>-

    @yield('content')

</div>
<script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
</body>
</html>