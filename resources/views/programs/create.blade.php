@extends('layout')

@section('content')
    <div class="card uper">
        <div class="card-header">
            Program Ekle
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>

                </div>
            @endif
            <form method="post" action="{{route('programs.store')}}">
                <div class="form-group">
                    @csrf
                    <label for="program_name">Program Adı</label>
                    <input type="text" name="program_name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="program_price">Program Fiyatı</label>
                    <input type="text" name="program_price" class="form-control">
                </div>
                <div class="form-group">
                    <label for="program_group">Program Grubu</label>
                    <input type="text" name="program_group" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">kaydet</button>
            </form>
        </div>
    </div>
@endsection