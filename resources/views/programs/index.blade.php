@extends('layout')

@section('content')

    <div class="uper">
        @if(session('success'))
            <div class="alert alert alert-success">
                {{session('success')}}
            </div>
            @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Program Adı</td>
                <td>Program Fiyatı</td>
                <td>Program Grubu</td>
                <td colspan="2"> İşlemler</td>

            </tr>

            </thead>
            <tbody>
            @foreach($programs as $program)
                <tr>
                    <td>{{ $program->program_name  }} </td>
                    <td>{{ $program->program_price  }}</td>
                    <td>{{ $program->program_group  }}</td>
                   <td>
                       <a class="btn btn-primary" href="{{ route('programs.edit',$program->id) }}">Düzenle</a>
                   </td>
                    <td>
<form action="{{route('programs.destroy',$program->id)}}" method="post">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Sil</button>
</form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endsection