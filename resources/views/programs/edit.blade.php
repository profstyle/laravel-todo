@extends('layout')

@section('content')


    <div class="card uper">
    <div class="card-header">
        Program Düzenle
    </div>
        <div class="card-body">

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>

                </div>
            @endif

                <form method="post" action="{{route('programs.update',$program->id)}}">
                @method('PATCH')
                    @csrf
                    <div class="form-group">
                        <label for="program_name">Program Adı</label>
                        <input type="text" name="program_name" class="form-control" value="{{$program->program_name}}">
                    </div>
                    <div class="form-group">
                        <label for="program_price">Program Fiyatı</label>
                        <input type="text" name="program_price" class="form-control" value="{{$program->program_price}}">
                    </div>
                    <div class="form-group">
                        <label for="program_group">Program Grubu</label>
                        <input type="text" name="program_group" class="form-control" value="{{$program->program_group}}">
                    </div>

                    <button type="submit" class="btn btn-primary">Güncelle</button>

                </form>
        </div>
    </div>
@endsection