<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', array('as'=>'index','uses'=>'ProgramController@index'));
Route::get('locale/{locale?}',array('as'=>'set-locale','uses'=>'AppController@setLocale'));
Route::get('/profile', array('as'=>'profile','uses'=>'UserController@getProfile'));
Route::resource('programs','ProgramController');